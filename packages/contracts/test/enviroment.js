const expect = require('chai').expect;
const ethers = require('ethers')
const ganache = require('ganache-cli');
const TOKEN_JSON = require('./../src/compiled_contracts/Token.json')
const Deployer = require('./../dist/index').Deployer
const CONSTANTS = require('./../dist/index').CONSTANTS

const getProvider = (network) => {
    switch (network) {
        case 'local':
            return new ethers.providers.JsonRpcProvider('HTTP://127.0.0.1:7545')
        case 'cli':
            return new ethers.providers.Web3Provider(
                ganache.provider({
                    "mnemonic": 'popular stock metal artefact nasty venture oblige horror face glory predict coach',
                    "gasLimit": 8000000,
                    // "gasPrice": 0,
                    // "hardfork": "petersburg",
                    // "default_balance_ether": 0,
                    // "network_id": 5799,
                    // "locked": false,
                    // "port": 5799,
                    // "total_accounts": 1,
                    // "unlocked_accounts": [],
                    "verbose": true,
                    "vmErrorsOnRPCResponse": true
                })
            )
        default:
            break;
    }

};

const provider = getProvider('cli');

describe('setup with deployer', async function () {
    this.timeout(9000);


    it("Setup accounts", async () => {
        this.DEVELOPER_SIGNER = ethers.Wallet.fromMnemonic('popular stock metal artefact nasty venture oblige horror face glory predict coach').connect(provider)
        this.DEVELOPER = await this.DEVELOPER_SIGNER.getAddress()
    })


    it('setup system', async () => {
        const deployer = new Deployer(this.DEVELOPER_SIGNER)
        const exchanges = await deployer.setup()
        expect(exchanges).length.to.be.greaterThan(1)
    });

})

xdescribe('setup without deployer', async function () {
    this.timeout(9000);


    it("Setup accounts", async () => {
        this.DEVELOPER_SIGNER = ethers.Wallet.fromMnemonic('weapon stamp galaxy acquire copy ready soft pole depart tool task blind').connect(provider)
        this.DEVELOPER = await this.DEVELOPER_SIGNER.getAddress()
    })


    it('Deploy EXCHANGE_CONTRACT', async () => {
        const exchangeContractFactory = new ethers.ContractFactory(CONSTANTS.exchangeByteAbi, CONSTANTS.exchangeByteCode, this.DEVELOPER_SIGNER);
        this.EXCHANGE_CONTRACT = await exchangeContractFactory.deploy();
        await this.EXCHANGE_CONTRACT.deployed();
        console.log("EXCHANGE_CONTRACT deployed => ", this.EXCHANGE_CONTRACT.address)
    });

    it('Deploy FACTORY_CONTRACT', async () => {
        const factoryContractFactory = new ethers.ContractFactory(CONSTANTS.factoryAbi, CONSTANTS.factoryByteCode, this.DEVELOPER_SIGNER);
        this.FACTORY_CONTRACT = await factoryContractFactory.deploy();
        await this.FACTORY_CONTRACT.deployed();
        console.log("FACTORY_CONTRACT deployed => ", this.FACTORY_CONTRACT.address)
    });

    it('setup tokenFactory', async () => {
        this.TOKEN_FACTORY = new ethers.ContractFactory(TOKEN_JSON.abi, TOKEN_JSON.bytecode, this.DEVELOPER_SIGNER);
        console.log("TOKEN_FACTORY has been setup")
    });

    it('Deploy TOKEN_ONE', async () => {
        this.TOKEN_ONE = await this.TOKEN_FACTORY.deploy("FIRST TOKEN", "ONE");
        await this.TOKEN_ONE.deployed();
        console.log('TOKEN_ONE deployed => ', this.TOKEN_ONE.address);
        console.log("TOKEN_ONE balance developer => ", (await this.TOKEN_ONE.balanceOf(this.DEVELOPER)).toString())
    });

    it('init Factory', async () => {
        const tx = await this.FACTORY_CONTRACT.initializeFactory(this.EXCHANGE_CONTRACT.address);
        await tx.wait();
    });


    it('create Exchange', async () => {
        await this.FACTORY_CONTRACT.createExchange(this.TOKEN_ONE.address, { gasLimit: 8000000 });
        this.TOKEN_ONE_EXCHANGE_ADDRESS = await this.FACTORY_CONTRACT.getExchange(this.TOKEN_ONE.address);
        console.log("TOKEN_ONE_EXCHANGE_ADDRESS => ", this.TOKEN_ONE_EXCHANGE_ADDRESS)

        this.TOKEN_ONE_EXCHANGE = new ethers.Contract(this.TOKEN_ONE_EXCHANGE_ADDRESS, CONSTANTS.exchangeByteAbi, this.DEVELOPER_SIGNER);


        await this.TOKEN_ONE.approve(this.TOKEN_ONE_EXCHANGE_ADDRESS, await this.TOKEN_ONE.balanceOf(this.DEVELOPER));
        console.log("Allowance for TOKEN ONE in exchange => ", (await this.TOKEN_ONE.allowance(this.DEVELOPER, this.TOKEN_ONE_EXCHANGE_ADDRESS)).toString())
    });

    it('add Liquidity', async () => {
        const tx = await this.TOKEN_ONE_EXCHANGE.addLiquidity(ethers.utils.parseEther('1'), ethers.utils.parseEther('1'), 1739591241, { value: ethers.utils.parseEther('1'), gasLimit: 8000000 });
        await tx.wait();
    });


})