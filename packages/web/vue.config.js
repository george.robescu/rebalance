const path = require('path');
module.exports = {
    chainWebpack: config => {
        config.resolve.set('symlinks', false), // Too avoid eslint trying to eslint imported packages
            config.module // To avoid eslint trying to get config from root lerna folder
                .rule('eslint')
                .use('eslint-loader')
                .tap(options => {
                    options.configFile = path.resolve(__dirname, ".eslintrc.js");
                    return options;
                })
    },
    configureWebpack: {
        devtool: 'source-map'
    },
    transpileDependencies: [
        /\bvue-awesome\b/
    ],
    publicPath: process.env.NODE_ENV === 'production'
        ? '/'
        : '/'
}