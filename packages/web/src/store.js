import Vue from 'vue'
import Vuex from 'vuex'

import metamask from './stores/metamask.store';
import rebalance from './stores/rebalance.store';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    metamask, rebalance
  },
})
