import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'

// Bootstrap
import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue)
// import 'bootstrap/dist/css/bootstrap.css'
// import 'bootstrap-vue/dist/bootstrap-vue.css'

//Slider
import VueSlider from 'vue-slider-component'
// import 'vue-slider-component/theme/default.css'

// Icons
import Icon from 'vue-awesome/components/Icon'
Vue.component('v-icon', Icon)
import 'vue-awesome/icons/caret-down'

Vue.component('VueSlider', VueSlider)

Vue.config.productionTip = false
Vue.prototype.$debug = true


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
