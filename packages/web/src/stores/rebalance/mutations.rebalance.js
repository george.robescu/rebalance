import Vue from "vue";

export const mutations = {
    // Mutations should always take HEX when storing numbers
    addExchangesToNetwork(state, data) {
        Vue.set(state.tokenExchangesSupported, data.network, data.exchanges)
    },
    addRebalanceContractAddress(state, { network, address }) {
        Vue.set(state.rebalanceContractAddress, network, address)
    },
    ready(state, data) {
        state.ready = data
    },
    tryAgain(state, data) {
        state.tryAgain = data
    },
    totalValueUSD(state, data) {
        state.totalValueUSD = data
    },
    totalValueETH(state, data) {
        state.totalValueETH = data._hex
    },
    addMessage(state, { message }) {
        state.messages.push(message)
    },
    rebalanceContractAddress(state, data) {
        state.rebalanceContractAddress = data
    },
    addToken(state, data) {
        Vue.set(state.tokens, data.tokenAddress, data)
        state.tokenList.push(data.tokenAddress)
    },
    addGroup(state, data) {
        Vue.set(state.groups, data.id, data)
        state.groupList.push(data.id)
    },
    setGroupCurrentBalancePercentage(state, { groupId, number }) {
        Vue.set(state.groups[groupId], 'currentBalancePercentage', number)
    },
    setGroupDesiredBalancePercentage(state, { groupId, number }) {
        Vue.set(state.groups[groupId], 'desiredBalancePercentage', number)
    },
    setGroupDesiredBalanceEth(state, { groupId, hex }) {
        Vue.set(state.groups[groupId], 'desiredBalanceEth', hex)
    },
    setGroupCurrentBalanceEth(state, { groupId, hex }) {
        Vue.set(state.groups[groupId], 'currentBalanceEth', hex)
    },
    setGroupDesiredBalanceUSD(state, { groupId, number }) {
        Vue.set(state.groups[groupId], 'desiredBalanceUSD', number)
    },
    setGroupCurrentBalanceUSD(state, { groupId, number }) {
        Vue.set(state.groups[groupId], 'currentBalanceUSD', number)
    },
    setTokenDesiredBalanceEth(state, { tokenAddress, hex }) {
        Vue.set(state.tokens[tokenAddress], 'desiredBalanceEth', hex)
    },
    setTokenDesiredBalancePercentage(state, { tokenAddress, number }) {
        Vue.set(state.tokens[tokenAddress], 'desiredBalancePercentage', number)
    },
    setTokenCurrentBalancePercentage(state, { tokenAddress, number }) {
        Vue.set(state.tokens[tokenAddress], 'currentBalancePercentage', number)
    },
    setTokenCurrentBalanceUSD(state, { tokenAddress, number }) {
        Vue.set(state.tokens[tokenAddress], 'currentBalanceUSD', number)
    },
    setTokenDesiredBalanceUSD(state, { tokenAddress, number }) {
        Vue.set(state.tokens[tokenAddress], 'desiredBalanceUSD', number)
    },
    setGroupToken(state, { groupId, tokenAddress }) {
        state.groups[groupId].tokens.push(tokenAddress)
    },
    removeGroupToken(state, { groupId, tokenAddress }) {
        const index = state.groups[groupId].tokens.indexOf(tokenAddress)
        if (index !== -1) {
            state.groups[groupId].tokens.splice(index, 1)
        }
    },
    setAllowance(state, { tokenAddress, hex }) {
        Vue.set(state.tokens[tokenAddress], 'allowance', hex)
    },
    setTopTokenInfo(state, { tokens }) {
        state.topTokensInfo = tokens
    },
    ethPriceUSD(state, { rate }) {
        state.ethPriceUSD = rate
    },
}