# Rebalance.to – Your secure gateway to decentralized finance

* Easy access to interest-bearing assets, geared investments or even automated asset management strategies
* Save fees, time and hassle with one-click rebalancing
* No tech jargon. Only focus on benefits and results

Just connect with Metamask, choose the assets you want, adjust your balances, and click “Rebalance.”

[**Use it online now**](https://app.rebalance.to/)

# Why: IDEO hackathon

Rebalance.to is our submission to the IDEO + CoinList Hackathon: `Making Blockchains Useful and Usable`. We were challenged to bring more people into crypto by radically simplifying the user experiences. To answer this, we asked the world about barriers to crypto-adoption. This survey was conducted by [Blockchangers](http://blockchangers.com) as part of an international effort to lower the obstacles against the use of cryptocurrencies. Learn more on [IDEO](https://coinlist.co/build/ideo).

As a group, respondents identify **many significant barriers**. The risk of fraud and the lack of legal frameworks is indeed the top concern. The second tallest barrier is the difficulties of buying crypto (aka “fiat swap”). It should be no surprise that people when asked about barriers to adoption, focus on the barriers **to entry**.

Nevertheless, if people fail to enter, it may be because the reasons for doing so are insufficiently compelling. Our respondents tend to agree. They put a high score also on the various difficulties one encounter when using crypto, including the pain of understanding and using decentralized financial products. **Easy to use financial products** are not the most significant barrier to the use of crypto, but the scores are indeed substantial and a substantial concern of our respondents. 

With all of this in mind, we set out to simplify the DeFi UX to get more people into crypto. So we planned, architectured, and built Rebalance. A safe and straightforward approach to decentralized finance to break the barries of entry to crypto. 

*[To learn more about our findings, read our full report](https://medium.com/blockchangers/how-to-create-enthusiasm-about-cryptocurrencies-f140aeec056). [And further reading here: "How come cryptocurrencies do not yet rule the world?"](https://medium.com/blockchangers/how-come-cryptocurrencies-do-not-yet-rule-the-world-9e0c8997053b)*

## Adoption

Initially, our goal was to track all of the millions in value being transferred on rebalance.to.  Some people [have sent their hard-earned cryptocurrencies to our contract and have rebalanced using our service](https://etherscan.io/address/0x1c3031e02e9a0ec237f5bd674eaaa5e56a2ae174), however, there's no way of sugarcoating this. Our learning is that it is tough to get people to use and trust our service with so little time. Building trust takes time, and we have been unable to gain massive adoption in the two weeks. 

Since it's hard to get people's trust in such a short time, we shifted to measuring traction online and on social media. We have got great interest and have seen [321% increase in unique users](https://imgur.com/a/7sz1rfP) to our landing page when we released our service on 20 September. 

We have also gotten lots of endorsements by the community as a whole. We have gotten [interest and positive response by the acclaimed ethereum personality Eric Conner](https://twitter.com/econoar/status/1175136371683381249). [Retweets by Ethereum core developers](https://twitter.com/antiprosynth), and the [massive support on Twitter with 1.700 likes](https://twitter.com/JonRamvi/status/1174781815149813762)!

All evidence about our traction has been linked to above.

## Video

![](https://imgur.com/download/Rnu9Nvx)

**[Check out the video demo of Rebalance.to!](https://www.youtube.com/watch?v=vZGOYLzEiAo)**

# Development WEB package🤓

* You need [metamask](https://metamask.io/) installed in your browser. ❄ 
* You need to be connected to an Ethereum network. I suggest [Ganache](https://www.trufflesuite.com/ganache ) for local development. 🤳
  * Use these settings in Ganache
    * Account and keys
      * ACCOUNT DEFAULT BALANCE = 100000 (because we use somewhere around 700 ETH as liquidity when setting up Uniswap exchanges)
      * TOTAL ACCOUNTS TO GENERATE = 1 (because when rebalancing you probably need to use Account 2 so you don't own ALL possible ETH and Tokens in the local development network)
    * Chain
      * `GasLimit = 8000000` (because...)
    * Advanced
      * `Verbose logs = true` (always good to be able to debug)

1. `git clone git@gitlab.com:blockchangers/rebalance.git`
2. `cd rebalance`
3. `npm run setup`
4. `npm run web`
5. Go to http://localhost:8080/
